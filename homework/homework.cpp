#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;

int main()
{
	const int N = 3;

	int sum = 0;
	time_t t;
	time(&t);
	int k = (localtime(&t)->tm_mday) %N;

   
	int array[N][N];

	for (int i = 0; i < N; i++)
	{
		for  (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;

			if (i == k) sum += array[i][j];
			
		}
	}

	for (int i = 0; i < N; i++)
	{
		if (i == k) {
			cout << sum << endl;
			continue;
		}
		for (int j = 0; j < N; j++)
		{
			cout << array[i][j] << " ";
		}
		cout << endl;
	}
	
	return 0;
}

